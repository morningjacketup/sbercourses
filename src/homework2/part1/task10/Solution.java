package homework2.part1.task10;

import java.util.Random;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        start();
    }

    public static void start() {
        Random random = new Random();
        int randomNumber = random.nextInt(0,1000);

        Scanner console = new Scanner(System.in);
        int number = console.nextInt();

        while (number != randomNumber) {
            if (number < randomNumber) {
                System.out.println("Это число меньше загаданного");
            }
            if (number > randomNumber) {
                System.out.println("Это число больше загаданного");
            }
            number = console.nextInt();
        }
        if (number == randomNumber) {
            System.out.println("Победа");
        }
    }
}

