package homework2.part1.doptask1;

import java.util.Random;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        int N = console.nextInt();

        while (N < 8) {
            System.out.println("Пароль с " + N + " количеством символов небезопасен");
            N = console.nextInt();
        }

        Random random = new Random();
    }
}
