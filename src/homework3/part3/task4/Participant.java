package homework3.part3.task4;

public record Participant(String name) {
    @Override
    public String name() {
        if (name == null) {
            System.err.println("Имя не задано");
        }
        return name;
    }

}
