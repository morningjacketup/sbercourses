package homework3.part3.task4;


public class Dog {
    private final String name;
    private Integer[] points;
    public String getName() {
        return name;
    }

    public Integer[] getPoints() {
        return points;
    }

    public void setPoints(Integer[] points) {
        this.points = points;
    }

    public Dog(String name) {
        this.name = name;
    }
}

