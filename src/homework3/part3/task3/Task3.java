package homework3.part3.task3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Task3 {
    public static void main(String[] args) {
        System.out.println(generateArray(2, 2));
        System.out.println(generateArray(3, 5));
    }

    public static ArrayList<ArrayList<Integer>> generateArray(int a, int b) {
        ArrayList<ArrayList<Integer>> arrayList = new ArrayList<>();

        for (int i = 0; i < b; i++) {
            ArrayList<Integer> secondArray = new ArrayList<>();
            for (int j = 0; j < a; j++) {
                secondArray.add(i + j);
            }
            arrayList.add(secondArray);
        }
        return arrayList;
    }
}
