package homework3.part3.task1;

public class GoldFish extends Fish implements Swimming{

    @Override
    public void swim() {
        System.out.println("I am swimming slowly");
    }
}
