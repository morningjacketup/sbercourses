package homework3.part3.task1;

public abstract class Animal {
    public final void eat() {
        System.out.println("I am eating!");
    }

    public final void sleep() {
        System.out.println("I am sleeping!");
    }

    public abstract void wayOfBirth();
}
