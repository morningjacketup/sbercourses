package homework3.part3.task1;

public class Bat extends Bird implements Flying {

    @Override
    public void fly() {
        System.out.println("I am flying slowly");
    }
}
