package homework3.part3.task2;

public class Test {
    public static void main(String[] args) {
        BestCarpenterEver bestCarpenterEver = new BestCarpenterEver();
        Stool stool = new Stool();
        Table table = new Table();


        System.out.println(bestCarpenterEver.isFixible(stool));
        System.out.println(bestCarpenterEver.isFixible(table));
    }
}
