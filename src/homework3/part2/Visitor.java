package homework3.part2;

public class Visitor {
    String name;
    int id;
    int bookTaken;

    public Visitor(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBookTaken() {
        return bookTaken;
    }

    public void setBookTaken(int bookTaken) {
        this.bookTaken = bookTaken;
    }
}
