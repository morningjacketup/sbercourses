package homework3.part1.task2;

import java.util.Arrays;

public class StudentTest {
    public static void main(String[] args) {
        Student student1 = new Student("Ivan", "Savvinov", 5, 4, 5, 3, 5);
        Student student2 = new Student("Ayza", "Yakovleva", 2, 4, 5, 5, 5, 2, 5, 3, 4, 4);
        Student student3 = new Student("Maya", "Savvinova", 2, 2, 3, 5, 5, 1, 2, 3, 4, 5);


        System.out.println(Arrays.toString(student1.getGrades()));
        System.out.println(Arrays.toString(student2.getGrades()));
        System.out.println(Arrays.toString(student3.getGrades()));

        System.out.println(student1.getAverageGrade());
        System.out.println(student2.getAverageGrade());
        System.out.println(student3.getAverageGrade());

        student1.addGrade(3);
        student2.addGrade(5);
        student3.addGrade(2);

        System.out.println(Arrays.toString(student1.getGrades()));
        System.out.println(Arrays.toString(student2.getGrades()));
        System.out.println(Arrays.toString(student3.getGrades()));

        student2.setSurname("Savvinova");
        System.out.println(student2.getSurname());

        student1.setName("Vanya");
        System.out.println(student1.getName());

        student1.setGrades(5, 5, 5, 5, 5, 5, 5, 5, 5, 5);
        System.out.println(Arrays.toString(student1.getGrades()));
    }
}
