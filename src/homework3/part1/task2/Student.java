package homework3.part1.task2;

import java.util.Arrays;

public class Student {
    private String name;
    private String surname;
    private int[] grades;

    public Student(String name, String surname, int... grades) {
        this.name = name;
        this.surname = surname;
        fillGrades(grades);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setGrades(int... grades) {
        fillGrades(grades);
    }

    public void addGrade(int grade) {
        for (int i = 1; i < grades.length - 1; i++)
            grades[i - 1] = grades[i];
        grades[grades.length - 1] = grade;
    }

    public double getAverageGrade() {
        double sum = 0;
        int countOfNulls = 0;
        for (int i = 1; i < grades.length; i++) {
            if (grades[i] == 0) {
                countOfNulls++;
            } else {
                sum += grades[i];
            }
        }
        return sum / (grades.length - countOfNulls);
    }

    private void fillGrades(int[] grades) {
        this.grades = new int[10];
        if (grades.length == this.grades.length) {
            this.grades = grades;
        } else if (grades.length < this.grades.length) {
            for (int i = 0; i < grades.length; i++) {
                 this.grades[i + 10 - grades.length] = grades[i];
            }
        } else {
            for (int i = 0; i < this.grades.length; i++) {
                this.grades[i] = grades[i + grades.length - 10];
            }
        }
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", grades=" + Arrays.toString(grades) +
                '}';
    }
}
