package homework3.part1.task1;

import java.util.Random;

public class Cat {
    private void sleep() {
        System.out.println("Sleep");
    }
    private void meow() {
        System.out.println("Meow");
    }
    private void eat() {
        System.out.println("Eat");
    }
    public void status() {
        Random random = new Random();
        int sum = random.nextInt(1, 4);

        switch (sum) {
            case 1:
                sleep();
            case 2:
                meow();
            case 3:
                eat();
        }
    }
}
