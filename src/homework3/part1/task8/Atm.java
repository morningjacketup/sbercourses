package homework3.part1.task8;

public class Atm {
    double roublesToDollars;
    double dollarsToRubles;
    private static int counter = 0;

    public Atm(double roublesToDollars, double dollarsToRubles) {
        this.roublesToDollars = roublesToDollars;
        this.dollarsToRubles = dollarsToRubles;
        counter++;
    }

    public double exchangeRoublesToDollars(double roubles) {
        return roubles * roublesToDollars;
    }

    public double exchangeDollarsToRoubles(double dollars) {
        return dollars * dollarsToRubles;
    }

    public int getCounter() {
        return counter;
    }
}
