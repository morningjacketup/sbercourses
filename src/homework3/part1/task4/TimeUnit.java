package homework3.part1.task4;

public class TimeUnit {
    private final int LOW_BOUND = 0;
    private final int MIDDAY_BOUND = 12;
    private final int HOURS_BOUND = 24;
    private final int MIN_AND_SEC_BOUND = 60;
    int hours;
    int minutes;
    int seconds;

    public TimeUnit(int hours, int minutes, int seconds) {
        if (timeValidate(hours, minutes, seconds)) {
            this.hours = hours;
            this.minutes = minutes;
            this.seconds = seconds;
        }
    }

    public TimeUnit(int hours, int minutes) {
        this(hours, minutes, 0);
    }

    public TimeUnit(int hours) {
        this(hours, 0, 0);
    }

    public void time24HourPrint() {
        System.out.printf("%02d:%02d:02d\n", hours, minutes, seconds);
    }

    public void time12HourPrint() {
        if (hours < MIDDAY_BOUND) {
            System.out.printf("%02d:%02d:%02d am\n", hours == LOW_BOUND ? MIDDAY_BOUND : hours, minutes, seconds);
        } else {
            System.out.printf("%02d:%02d:%02d pm\n", hours == MIDDAY_BOUND ? MIDDAY_BOUND : hours, minutes, seconds);
        }
    }

    public void timeAdd(int hours, int minutes, int seconds) {
        if (timeValidate(hours, minutes, seconds)) {
            this.hours = (this.hours + hours + (this.minutes + minutes + (this.seconds + seconds)
                    / MIN_AND_SEC_BOUND) / MIN_AND_SEC_BOUND) % HOURS_BOUND;
            this.minutes = (this.minutes + minutes + (this.seconds + seconds) / MIN_AND_SEC_BOUND) % MIN_AND_SEC_BOUND;
            this.seconds = (this.seconds + seconds) % MIN_AND_SEC_BOUND;
        }
    }

    private boolean timeValidate(int hours, int minutes, int seconds) {
        if (hours < LOW_BOUND || hours > HOURS_BOUND || minutes < LOW_BOUND || minutes >= MIN_AND_SEC_BOUND
                || seconds < LOW_BOUND || seconds >= MIN_AND_SEC_BOUND) {
            System.out.println("Данные времени некорректны");
            return false;
        }
        return true;
    }
}
