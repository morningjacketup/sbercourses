package homework3.part1.task7;

public class TriangleChecker {

    public static boolean isTriangleExists(double a, double b, double c) {
        if (a + b < c || b + c < a) {
            return false;
        }
        return true;
    }
}
