package homework3.part1.task3;

import homework3.part1.task2.Student;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        Student[] students = {
                new Student("Petya", "Pupkin", 5, 5, 5, 5, 5, 5, 5, 5, 5),
                new Student("Ivan", "Savvinov", 2, 3, 4, 5, 2, 3, 4, 5, 2),
                new Student("Ayza", "Alekseeva", 5, 5, 4, 5, 4, 3, 4, 5, 2),
                new Student("Maya", "Zapkin", 5, 5, 3, 1, 2, 3, 4, 5, 2),
                new Student("Alex", "Ivanov", 1, 2, 2, 3, 4, 3, 2, 3, 2),
                new Student("Ivan", "Bright", 2, 3, 4, 5, 2, 3, 4, 5, 2),
        };


        System.out.println(StudentService.bestStudent(students).getName());

        System.out.println(Arrays.toString(students));
        StudentService.sortBySurname(students);
        System.out.println(Arrays.toString(students));
    }
}
