package homework3.part1.task5;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        DayOfWeek week = new DayOfWeek();
        DayOfWeek week2 = new DayOfWeek();
        DayOfWeek week3 = new DayOfWeek();
        DayOfWeek week4 = new DayOfWeek();
        DayOfWeek week5 = new DayOfWeek();
        DayOfWeek week6 = new DayOfWeek();
        DayOfWeek week7 = new DayOfWeek();

        week.setId((byte) 1);
        week.setWeek("Monday");
        week2.setId((byte) 2);
        week2.setWeek("Tuesday");
        week3.setId((byte) 3);
        week3.setWeek("Wednesday");
        week4.setId((byte) 4);
        week4.setWeek("Thursday");
        week5.setId((byte) 5);
        week5.setWeek("Friday");
        week6.setId((byte) 6);
        week6.setWeek("Saturday");
        week7.setId((byte) 7);
        week7.setWeek("Sunday");

        List<DayOfWeek> weeks = new ArrayList<>();
        weeks.add(week);
        weeks.add(week2);
        weeks.add(week3);
        weeks.add(week4);
        weeks.add(week5);
        weeks.add(week6);
        weeks.add(week7);

        for (DayOfWeek day: weeks
             ) {
            System.out.println(day);
        }
    }
}
