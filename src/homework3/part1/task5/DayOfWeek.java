package homework3.part1.task5;

public class DayOfWeek {
    byte id;
    String week;

    public byte getId() {
        return id;
    }

    public void setId(byte id) {
        this.id = id;
    }

    public String getWeek() {
        return week;
    }

    public void setWeek(String week) {
        this.week = week;
    }

    @Override
    public String toString() {
        return id + " " + week;
    }
}
